# Bazy danych - programowanie (JDBC, Hibernate)

# Część 1 - JDBC
- [prezentacja 1](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/materia%C5%82y/JDBC/jdbc.pdf)
- [zadania JDBC](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/materia%C5%82y/JDBC/Zadania%20-%20JDBC.txt)

  
- przykłady dostępne w projekcie: [klasy w pakietach jdbc, commons](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/src/main/java/com/sda/jdbc/) oraz [testy](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/src/test/java/com/sda/jdbc/examples/)
  
  
- JDBC Tutorial: https://www.javatpoint.com/java-jdbc
- JDBC Tutorial https://www.tutorialspoint.com/jdbc/index.htm

  
- połaczenie z bazą danych: http://tutorials.jenkov.com/jdbc/connection.html
- przygotowanie i wykonanie zapytania:  https://www.tutorialspoint.com/jdbc/jdbc-statements.htm
- przetwarzanie wyniku zapytania: https://www.baeldung.com/jdbc-resultset
- zamknięcie połączenia: https://mkyong.com/java/try-with-resources-example-in-jdk-7/

# Część 2 - Hibernate
- [prezentacja 2](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/materia%C5%82y/Hibernate/hibernate.pdf)
- [zadania Hibernate](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/materia%C5%82y/Hibernate/Zadania%20-%20Hibernate.txt)

  
- przykłady dostępne w projekcie: [klasy w pakiecie hibernate](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/src/main/java/com/sda/hibernate/commons/) oraz [testy](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/src/test/java/com/sda/hibernate/examples/)

  
- Hibernate Tutorial - https://www.javatpoint.com/hibernate-tutorial
- Hibernate Tutorial - https://www.tutorialspoint.com/hibernate/index.htm
- Hibernate Tutorial - http://www.mkyong.com/tutorials/hibernate-tutorials/

# Szczegóły projektu _jdbc-hibernate-examples_ 
Do uruchomienia projektu wymagane jest wykonanie następujących kroków:
1. sklonowanie projektu jdbc-hibernate-examples przy pomocy git
2. import baz danych [hr_hibernate](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/materia%C5%82y/JDBC/hr_postgres.sql) oraz [cinema_hibernate](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/materia%C5%82y/Hibernate/cinema_hibernate_postgres.sql)
3.  uruchomienie projektu w Intellij IDEA
4. upewnić się, że w Intellij jest zainstalowany i właczony plugin [lombok](https://www.baeldung.com/lombok-ide)
5. wykonać polecenie mvn clean install w projekcie

W rezultacie budowania projektu duża liczba testów powinna zakończyć się niepowodzeniem. Jednak jest to oczekiwany efekt.  
Naszym zadaniem na zajęciach będzie uzupełnienie brakujących części implementacji przy pomocy zagadnień z JDBC i Hibernate.
Nad testami będziemy pracować przy pomocy podejścia TDD i będziemy dążyć do tego aby ich uruchomienie kończyło się sukcesem.
Klasy testowe zawierające zadania to [ZadaniaJDBC](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/src/test/java/com/sda/jdbc/todo/ZadaniaJDBC.java) i [ZadaniaHibernate](https://bitbucket.org/pabloo99/jdbc-hibernate-examples/src/master/src/test/java/com/sda/hibernate/todo/ZadaniaHibernate.java).


