package com.sda.jdbc.examples.postgres;

import com.sda.jdbc.commons.connection.CustomConnection;
import com.sda.jdbc.commons.connection.PostgresConnector;
import com.sda.jdbc.commons.entity.Country;
import com.sda.jdbc.examples.CountryDAO;
import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class CountryJdbcDaoPostgresTest {

    private CountryDAO countryDAO;
    private CustomConnection postgresConnection;

    @BeforeClass
    public void setUp() {
        postgresConnection = new PostgresConnector();
        countryDAO = new CountryDAO(postgresConnection);
    }

    @Test
    public void shouldReturnCountryByName(){
        List<Country> countries = new ArrayList<>(countryDAO.findCountryByName("Germany"));

        log.info("Countries count: " + countries.size());

        Assert.assertEquals(countries.size(), 1);
    }
}