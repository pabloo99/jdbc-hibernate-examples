package com.sda.jdbc.examples.oracle;

import com.sda.jdbc.commons.connection.CustomConnection;
import com.sda.jdbc.commons.connection.OracleConnector;
import com.sda.jdbc.commons.entity.Country;
import com.sda.jdbc.examples.CountryDAO;
import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class CountryJdbcDaoOracleTest {

    private CountryDAO countryDAO;
    private CustomConnection oracleConnection;

    @BeforeClass
    public void setUp() {
        oracleConnection = new OracleConnector();
        countryDAO = new CountryDAO(oracleConnection);
    }

    @Test
    public void shouldReturnCountryByName(){
        List<Country> countries = new ArrayList<>(countryDAO.findCountryByName("Germany"));

        log.info("Countries count: " + countries.size());

        Assert.assertEquals(countries.size(), 1);
    }
}