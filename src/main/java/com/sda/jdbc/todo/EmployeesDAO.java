package com.sda.jdbc.todo;


import com.sda.jdbc.commons.entity.Employee;

import java.util.List;

public class EmployeesDAO {

    public EmployeesDAO() {
    }

    public Employee findById(int employeeId) {
        return null;
    }

    public void delete(Employee employee) {
    }

    public void save(Employee employee) {
    }

    public void update(Employee employee) {
    }

    public List<Employee> findAll() {
        return null;
    }

    public void saveBatch(List<Employee> employees) {
    }

    public void deleteBatch(List<Employee> employees) {
    }

}
