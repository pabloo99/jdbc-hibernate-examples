package com.sda.jdbc.examples;

import com.sda.jdbc.commons.connection.CustomConnection;
import com.sda.jdbc.commons.entity.Country;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Log4j2 // więcej informacji na temat tej adnotacji: https://projectlombok.org/features/log
public class CountryDAO {

    private final CustomConnection connector;

    public CountryDAO(CustomConnection connector) {
        this.connector = connector;
    }

    public List<Country> findCountryByName(String countryName) {
        List<Country> result = new ArrayList<>();

        String query = "SELECT * FROM countries WHERE country_name = ?";

        try (Connection connection = connector.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, countryName);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Country country = new Country();
                country.setCountryId(resultSet.getString("country_id"));
                country.setCountryName(resultSet.getString("country_name"));

                result.add(country);
            }

            resultSet.close();
        } catch (SQLException exception) {
            log.error(exception.getMessage());
        }

        return result;
    }
}
