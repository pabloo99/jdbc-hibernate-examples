--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

-- Started on 2022-03-06 16:43:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 210 (class 1259 OID 46619)
-- Name: countries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.countries (
    country_id integer NOT NULL,
    country_name character varying(128) NOT NULL,
    version integer
);


ALTER TABLE public.countries OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 46699)
-- Name: countries_aud; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.countries_aud (
    rev integer NOT NULL,
    revtype integer,
    country_name character varying(255) DEFAULT NULL::character varying,
    country_id integer NOT NULL
);


ALTER TABLE public.countries_aud OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 46618)
-- Name: countries_country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.countries ALTER COLUMN country_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.countries_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 212 (class 1259 OID 46625)
-- Name: genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genres (
    genre_id integer NOT NULL,
    genre_name character varying(128) NOT NULL
);


ALTER TABLE public.genres OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 46624)
-- Name: generes_genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.genres ALTER COLUMN genre_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.generes_genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 223 (class 1259 OID 72065)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 46674)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    role_id integer NOT NULL,
    role_name character varying(128) NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 46673)
-- Name: jobs_job_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.roles ALTER COLUMN role_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.jobs_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 214 (class 1259 OID 46631)
-- Name: movies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movies (
    movie_id integer NOT NULL,
    title character varying(256) NOT NULL,
    production_year integer,
    genre_id integer,
    country_id integer,
    duration integer
);


ALTER TABLE public.movies OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 46630)
-- Name: movies_movie_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.movies ALTER COLUMN movie_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.movies_movie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 216 (class 1259 OID 46647)
-- Name: persons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persons (
    person_id integer NOT NULL,
    first_name character varying(128),
    last_name character varying(128) NOT NULL,
    date_of_birth date,
    country_id integer
);


ALTER TABLE public.persons OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 46679)
-- Name: persons_movies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persons_movies (
    person_id integer NOT NULL,
    movie_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.persons_movies OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 46646)
-- Name: persons_person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persons_person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persons_person_id_seq OWNER TO postgres;

--
-- TOC entry 3382 (class 0 OID 0)
-- Dependencies: 215
-- Name: persons_person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persons_person_id_seq OWNED BY public.persons.person_id;


--
-- TOC entry 222 (class 1259 OID 46706)
-- Name: revinfo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.revinfo (
    rev integer NOT NULL,
    revtstmp bigint
);


ALTER TABLE public.revinfo OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 46705)
-- Name: revinfo_rev_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.revinfo_rev_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.revinfo_rev_seq OWNER TO postgres;

--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 221
-- Name: revinfo_rev_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.revinfo_rev_seq OWNED BY public.revinfo.rev;


--
-- TOC entry 3198 (class 2604 OID 46720)
-- Name: persons person_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons ALTER COLUMN person_id SET DEFAULT nextval('public.persons_person_id_seq'::regclass);


--
-- TOC entry 3200 (class 2604 OID 46709)
-- Name: revinfo rev; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.revinfo ALTER COLUMN rev SET DEFAULT nextval('public.revinfo_rev_seq'::regclass);


--
-- TOC entry 3363 (class 0 OID 46619)
-- Dependencies: 210
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.countries VALUES (3, 'Great Britain', 1);
INSERT INTO public.countries VALUES (1, 'Poland 1', 19);
INSERT INTO public.countries VALUES (37, 'Zimbabwe', 0);
INSERT INTO public.countries VALUES (38, 'Zimbabwe', 0);
INSERT INTO public.countries VALUES (39, 'Zimbabwe', 0);
INSERT INTO public.countries VALUES (40, 'Zimbabwe', 0);
INSERT INTO public.countries VALUES (41, 'Zimbabwe', 0);
INSERT INTO public.countries VALUES (42, 'Zimbabwe', 0);


--
-- TOC entry 3373 (class 0 OID 46699)
-- Dependencies: 220
-- Data for Name: countries_aud; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.countries_aud VALUES (1, 0, 'Nigeria', -30);
INSERT INTO public.countries_aud VALUES (2, 0, 'Nigeria', -29);
INSERT INTO public.countries_aud VALUES (3, 0, 'Nigeria', 21);
INSERT INTO public.countries_aud VALUES (4, 0, 'Nigeria', 22);
INSERT INTO public.countries_aud VALUES (5, 2, NULL, 22);
INSERT INTO public.countries_aud VALUES (6, 0, 'France', 23);
INSERT INTO public.countries_aud VALUES (7, 1, 'Poland 2', 1);
INSERT INTO public.countries_aud VALUES (8, 1, 'Poland 3', 1);
INSERT INTO public.countries_aud VALUES (9, 1, 'Poland 1', 1);
INSERT INTO public.countries_aud VALUES (10, 0, 'Nigeria', 27);
INSERT INTO public.countries_aud VALUES (11, 0, 'Nigeria', 28);
INSERT INTO public.countries_aud VALUES (12, 0, 'Nigeria', 29);
INSERT INTO public.countries_aud VALUES (13, 2, NULL, 29);
INSERT INTO public.countries_aud VALUES (14, 0, 'Nigeria', 30);
INSERT INTO public.countries_aud VALUES (15, 2, NULL, 30);
INSERT INTO public.countries_aud VALUES (16, 0, 'Nigeria', 31);
INSERT INTO public.countries_aud VALUES (17, 2, NULL, 31);
INSERT INTO public.countries_aud VALUES (18, 0, 'Nigeria', 32);
INSERT INTO public.countries_aud VALUES (19, 2, NULL, 32);
INSERT INTO public.countries_aud VALUES (20, 0, 'France', 33);
INSERT INTO public.countries_aud VALUES (21, 1, 'Poland 2', 1);
INSERT INTO public.countries_aud VALUES (22, 1, 'Poland 3', 1);
INSERT INTO public.countries_aud VALUES (23, 1, 'Poland 1', 1);
INSERT INTO public.countries_aud VALUES (24, 0, 'France', 34);
INSERT INTO public.countries_aud VALUES (25, 1, 'Poland 2', 1);
INSERT INTO public.countries_aud VALUES (26, 1, 'Poland 3', 1);
INSERT INTO public.countries_aud VALUES (27, 1, 'Poland 1', 1);
INSERT INTO public.countries_aud VALUES (28, 2, NULL, 34);
INSERT INTO public.countries_aud VALUES (29, 0, 'France', 35);
INSERT INTO public.countries_aud VALUES (30, 1, 'Poland 2', 1);
INSERT INTO public.countries_aud VALUES (31, 1, 'Poland 3', 1);
INSERT INTO public.countries_aud VALUES (32, 1, 'Poland 1', 1);
INSERT INTO public.countries_aud VALUES (33, 2, NULL, 35);
INSERT INTO public.countries_aud VALUES (34, 0, 'Nigeria', 36);
INSERT INTO public.countries_aud VALUES (35, 2, NULL, 36);
INSERT INTO public.countries_aud VALUES (36, 0, 'Zimbabwe', 37);
INSERT INTO public.countries_aud VALUES (37, 0, 'Zimbabwe', 38);
INSERT INTO public.countries_aud VALUES (38, 0, 'Zimbabwe', 39);
INSERT INTO public.countries_aud VALUES (39, 0, 'Zimbabwe', 40);
INSERT INTO public.countries_aud VALUES (40, 0, 'Zimbabwe', 41);
INSERT INTO public.countries_aud VALUES (41, 0, 'Zimbabwe', 42);
INSERT INTO public.countries_aud VALUES (42, 0, 'Zimbabwe', 43);
INSERT INTO public.countries_aud VALUES (43, 2, NULL, 43);
INSERT INTO public.countries_aud VALUES (44, 0, 'Zimbabwe', 44);
INSERT INTO public.countries_aud VALUES (45, 2, NULL, 44);
INSERT INTO public.countries_aud VALUES (46, 0, 'Zimbabwe', 45);
INSERT INTO public.countries_aud VALUES (47, 2, NULL, 45);
INSERT INTO public.countries_aud VALUES (48, 0, 'Zimbabwe', 46);
INSERT INTO public.countries_aud VALUES (49, 2, NULL, 46);
INSERT INTO public.countries_aud VALUES (50, 0, 'Zimbabwe', 47);
INSERT INTO public.countries_aud VALUES (51, 2, NULL, 47);
INSERT INTO public.countries_aud VALUES (52, 0, 'Zimbabwe', 48);
INSERT INTO public.countries_aud VALUES (53, 2, NULL, 48);
INSERT INTO public.countries_aud VALUES (54, 0, 'Zimbabwe', 49);
INSERT INTO public.countries_aud VALUES (55, 2, NULL, 49);
INSERT INTO public.countries_aud VALUES (56, 0, 'Zimbabwe', 50);
INSERT INTO public.countries_aud VALUES (57, 2, NULL, 50);
INSERT INTO public.countries_aud VALUES (58, 0, 'Zimbabwe', 51);
INSERT INTO public.countries_aud VALUES (59, 2, NULL, 51);
INSERT INTO public.countries_aud VALUES (60, 0, 'Zimbabwe', 52);
INSERT INTO public.countries_aud VALUES (61, 2, NULL, 52);
INSERT INTO public.countries_aud VALUES (62, 0, 'France', 53);
INSERT INTO public.countries_aud VALUES (63, 1, 'Poland 2', 1);
INSERT INTO public.countries_aud VALUES (64, 1, 'Poland 3', 1);
INSERT INTO public.countries_aud VALUES (65, 1, 'Poland 1', 1);
INSERT INTO public.countries_aud VALUES (66, 2, NULL, 53);
INSERT INTO public.countries_aud VALUES (67, 0, 'Nigeria', 54);
INSERT INTO public.countries_aud VALUES (68, 2, NULL, 54);
INSERT INTO public.countries_aud VALUES (69, 0, 'France', 55);
INSERT INTO public.countries_aud VALUES (70, 1, 'Poland 2', 1);
INSERT INTO public.countries_aud VALUES (71, 1, 'Poland 3', 1);
INSERT INTO public.countries_aud VALUES (72, 1, 'Poland 1', 1);
INSERT INTO public.countries_aud VALUES (73, 2, NULL, 55);
INSERT INTO public.countries_aud VALUES (74, 0, 'Nigeria', 56);
INSERT INTO public.countries_aud VALUES (75, 2, NULL, 56);


--
-- TOC entry 3365 (class 0 OID 46625)
-- Dependencies: 212
-- Data for Name: genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.genres VALUES (1, 'drama');
INSERT INTO public.genres VALUES (2, 'crime');
INSERT INTO public.genres VALUES (3, 'comedy');
INSERT INTO public.genres VALUES (4, 'thriller');
INSERT INTO public.genres VALUES (5, 'sci-fi');


--
-- TOC entry 3367 (class 0 OID 46631)
-- Dependencies: 214
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.movies VALUES (1, 'Titanic', 1997, 1, 1, NULL);
INSERT INTO public.movies VALUES (2, 'Forrest Gump', 1994, 1, 1, NULL);
INSERT INTO public.movies VALUES (3, 'Pulp Fiction', 1994, 2, 1, NULL);
INSERT INTO public.movies VALUES (4, 'Gran Torino', 2008, 1, 1, NULL);


--
-- TOC entry 3369 (class 0 OID 46647)
-- Dependencies: 216
-- Data for Name: persons; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.persons VALUES (1, 'Leonardo', 'Di Caprio', '1974-11-11', 1);
INSERT INTO public.persons VALUES (2, 'Tom', 'Hanks', '1956-07-09', 1);
INSERT INTO public.persons VALUES (3, 'Bruce', 'Willis', '1955-03-19', 1);
INSERT INTO public.persons VALUES (4, 'John', 'Travolta', '1954-02-18', 1);
INSERT INTO public.persons VALUES (5, 'Uma', 'Thurman', '1970-04-29', 1);
INSERT INTO public.persons VALUES (6, 'Kate', 'Winslet', '1975-10-05', 3);
INSERT INTO public.persons VALUES (7, 'Clint', 'Eastwood', '1930-05-31', 1);


--
-- TOC entry 3372 (class 0 OID 46679)
-- Dependencies: 219
-- Data for Name: persons_movies; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.persons_movies VALUES (1, 1, 1);
INSERT INTO public.persons_movies VALUES (6, 1, 1);
INSERT INTO public.persons_movies VALUES (2, 2, 1);
INSERT INTO public.persons_movies VALUES (3, 3, 1);
INSERT INTO public.persons_movies VALUES (4, 3, 1);
INSERT INTO public.persons_movies VALUES (5, 3, 1);
INSERT INTO public.persons_movies VALUES (7, 4, 1);
INSERT INTO public.persons_movies VALUES (7, 4, 2);
INSERT INTO public.persons_movies VALUES (7, 4, 3);


--
-- TOC entry 3375 (class 0 OID 46706)
-- Dependencies: 222
-- Data for Name: revinfo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.revinfo VALUES (1, 1645822125996);
INSERT INTO public.revinfo VALUES (2, 1645822312839);
INSERT INTO public.revinfo VALUES (3, 1645827065040);
INSERT INTO public.revinfo VALUES (4, 1645827141182);
INSERT INTO public.revinfo VALUES (5, 1645827141324);
INSERT INTO public.revinfo VALUES (6, 1646425747951);
INSERT INTO public.revinfo VALUES (7, 1646425748532);
INSERT INTO public.revinfo VALUES (8, 1646425748552);
INSERT INTO public.revinfo VALUES (9, 1646425748583);
INSERT INTO public.revinfo VALUES (10, 1646427095421);
INSERT INTO public.revinfo VALUES (11, 1646427219477);
INSERT INTO public.revinfo VALUES (12, 1646427495354);
INSERT INTO public.revinfo VALUES (13, 1646427495508);
INSERT INTO public.revinfo VALUES (14, 1646427646162);
INSERT INTO public.revinfo VALUES (15, 1646427646200);
INSERT INTO public.revinfo VALUES (16, 1646428309284);
INSERT INTO public.revinfo VALUES (17, 1646428309321);
INSERT INTO public.revinfo VALUES (18, 1646428457899);
INSERT INTO public.revinfo VALUES (19, 1646428457940);
INSERT INTO public.revinfo VALUES (20, 1646428484069);
INSERT INTO public.revinfo VALUES (21, 1646428484236);
INSERT INTO public.revinfo VALUES (22, 1646428484242);
INSERT INTO public.revinfo VALUES (23, 1646428484250);
INSERT INTO public.revinfo VALUES (24, 1646428528551);
INSERT INTO public.revinfo VALUES (25, 1646428528709);
INSERT INTO public.revinfo VALUES (26, 1646428528714);
INSERT INTO public.revinfo VALUES (27, 1646428528725);
INSERT INTO public.revinfo VALUES (28, 1646428528743);
INSERT INTO public.revinfo VALUES (29, 1646428559601);
INSERT INTO public.revinfo VALUES (30, 1646428559744);
INSERT INTO public.revinfo VALUES (31, 1646428559749);
INSERT INTO public.revinfo VALUES (32, 1646428559758);
INSERT INTO public.revinfo VALUES (33, 1646428559783);
INSERT INTO public.revinfo VALUES (34, 1646428560109);
INSERT INTO public.revinfo VALUES (35, 1646428560122);
INSERT INTO public.revinfo VALUES (36, 1646431035632);
INSERT INTO public.revinfo VALUES (37, 1646431131010);
INSERT INTO public.revinfo VALUES (38, 1646470436639);
INSERT INTO public.revinfo VALUES (39, 1646470539767);
INSERT INTO public.revinfo VALUES (40, 1646488503037);
INSERT INTO public.revinfo VALUES (41, 1646520852755);
INSERT INTO public.revinfo VALUES (42, 1646600824317);
INSERT INTO public.revinfo VALUES (43, 1646600824346);
INSERT INTO public.revinfo VALUES (44, 1646601101534);
INSERT INTO public.revinfo VALUES (45, 1646601101559);
INSERT INTO public.revinfo VALUES (46, 1646601384608);
INSERT INTO public.revinfo VALUES (47, 1646601384632);
INSERT INTO public.revinfo VALUES (48, 1646607068000);
INSERT INTO public.revinfo VALUES (49, 1646607068031);
INSERT INTO public.revinfo VALUES (50, 1646607358958);
INSERT INTO public.revinfo VALUES (51, 1646607358974);
INSERT INTO public.revinfo VALUES (52, 1646607478292);
INSERT INTO public.revinfo VALUES (53, 1646607478324);
INSERT INTO public.revinfo VALUES (54, 1646608114002);
INSERT INTO public.revinfo VALUES (55, 1646608114018);
INSERT INTO public.revinfo VALUES (56, 1646693730661);
INSERT INTO public.revinfo VALUES (57, 1646693730685);
INSERT INTO public.revinfo VALUES (58, 1646719770854);
INSERT INTO public.revinfo VALUES (59, 1646719770877);
INSERT INTO public.revinfo VALUES (60, 1646720433690);
INSERT INTO public.revinfo VALUES (61, 1646720433721);
INSERT INTO public.revinfo VALUES (62, 1646721172999);
INSERT INTO public.revinfo VALUES (63, 1646721173105);
INSERT INTO public.revinfo VALUES (64, 1646721173110);
INSERT INTO public.revinfo VALUES (65, 1646721173117);
INSERT INTO public.revinfo VALUES (66, 1646721173136);
INSERT INTO public.revinfo VALUES (67, 1646721173417);
INSERT INTO public.revinfo VALUES (68, 1646721173431);
INSERT INTO public.revinfo VALUES (69, 1646721440585);
INSERT INTO public.revinfo VALUES (70, 1646721440699);
INSERT INTO public.revinfo VALUES (71, 1646721440707);
INSERT INTO public.revinfo VALUES (72, 1646721440718);
INSERT INTO public.revinfo VALUES (73, 1646721440734);
INSERT INTO public.revinfo VALUES (74, 1646721441003);
INSERT INTO public.revinfo VALUES (75, 1646721441019);


--
-- TOC entry 3371 (class 0 OID 46674)
-- Dependencies: 218
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.roles VALUES (1, 'actor');
INSERT INTO public.roles VALUES (2, 'director');
INSERT INTO public.roles VALUES (3, 'producer');


--
-- TOC entry 3384 (class 0 OID 0)
-- Dependencies: 209
-- Name: countries_country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.countries_country_id_seq', 56, true);


--
-- TOC entry 3385 (class 0 OID 0)
-- Dependencies: 211
-- Name: generes_genre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.generes_genre_id_seq', 5, true);


--
-- TOC entry 3386 (class 0 OID 0)
-- Dependencies: 223
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 75, true);


--
-- TOC entry 3387 (class 0 OID 0)
-- Dependencies: 217
-- Name: jobs_job_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jobs_job_id_seq', 3, true);


--
-- TOC entry 3388 (class 0 OID 0)
-- Dependencies: 213
-- Name: movies_movie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movies_movie_id_seq', 40, true);


--
-- TOC entry 3389 (class 0 OID 0)
-- Dependencies: 215
-- Name: persons_person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persons_person_id_seq', 20, true);


--
-- TOC entry 3390 (class 0 OID 0)
-- Dependencies: 221
-- Name: revinfo_rev_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.revinfo_rev_seq', 1, false);


--
-- TOC entry 3214 (class 2606 OID 46704)
-- Name: countries_aud countries_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.countries_aud
    ADD CONSTRAINT countries_aud_pkey PRIMARY KEY (country_id, rev);


--
-- TOC entry 3202 (class 2606 OID 46623)
-- Name: countries countries_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pk PRIMARY KEY (country_id);


--
-- TOC entry 3204 (class 2606 OID 46629)
-- Name: genres generes_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genres
    ADD CONSTRAINT generes_pk PRIMARY KEY (genre_id);


--
-- TOC entry 3206 (class 2606 OID 46635)
-- Name: movies movies_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT movies_pk PRIMARY KEY (movie_id);


--
-- TOC entry 3212 (class 2606 OID 46683)
-- Name: persons_movies persons_movies_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons_movies
    ADD CONSTRAINT persons_movies_pk PRIMARY KEY (person_id, movie_id, role_id);


--
-- TOC entry 3208 (class 2606 OID 46722)
-- Name: persons persons_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons
    ADD CONSTRAINT persons_pk PRIMARY KEY (person_id);


--
-- TOC entry 3216 (class 2606 OID 46711)
-- Name: revinfo revinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.revinfo
    ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);


--
-- TOC entry 3210 (class 2606 OID 46678)
-- Name: roles roles_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pk PRIMARY KEY (role_id);


--
-- TOC entry 3217 (class 2606 OID 46636)
-- Name: movies fk_movie_genres; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT fk_movie_genres FOREIGN KEY (genre_id) REFERENCES public.genres(genre_id);


--
-- TOC entry 3218 (class 2606 OID 46641)
-- Name: movies fk_movies_countries; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT fk_movies_countries FOREIGN KEY (country_id) REFERENCES public.countries(country_id);


--
-- TOC entry 3219 (class 2606 OID 46653)
-- Name: persons persons_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons
    ADD CONSTRAINT persons_fk FOREIGN KEY (country_id) REFERENCES public.countries(country_id);


--
-- TOC entry 3222 (class 2606 OID 46723)
-- Name: persons_movies persons_movies_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons_movies
    ADD CONSTRAINT persons_movies_fk FOREIGN KEY (person_id) REFERENCES public.persons(person_id);


--
-- TOC entry 3220 (class 2606 OID 46689)
-- Name: persons_movies persons_movies_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons_movies
    ADD CONSTRAINT persons_movies_fk_1 FOREIGN KEY (movie_id) REFERENCES public.movies(movie_id);


--
-- TOC entry 3221 (class 2606 OID 46694)
-- Name: persons_movies persons_movies_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons_movies
    ADD CONSTRAINT persons_movies_fk_2 FOREIGN KEY (role_id) REFERENCES public.roles(role_id);


-- Completed on 2022-03-08 07:43:23

--
-- PostgreSQL database dump complete
--

