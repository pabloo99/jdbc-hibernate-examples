--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

-- Started on 2021-12-29 17:51:35


--
-- TOC entry 6 (class 2615 OID 415671)
-- Name: hr; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA hr;

ALTER SCHEMA hr OWNER TO postgres;


--
-- TOC entry 408 (class 1259 OID 415684)
-- Name: countries; Type: TABLE; Schema: hr; Owner: postgres
--

CREATE TABLE hr.countries (
    country_id character varying(2) NOT NULL,
    country_name character varying(40),
    region_id numeric(10,0)
);


ALTER TABLE hr.countries OWNER TO postgres;

--
-- TOC entry 411 (class 1259 OID 415704)
-- Name: departments; Type: TABLE; Schema: hr; Owner: postgres
--

CREATE TABLE hr.departments (
    department_id numeric(4,0) NOT NULL,
    department_name character varying(30) NOT NULL,
    manager_id numeric(6,0) DEFAULT NULL::numeric,
    location_id numeric(4,0) DEFAULT NULL::numeric
);


ALTER TABLE hr.departments OWNER TO postgres;

--
-- TOC entry 407 (class 1259 OID 415672)
-- Name: employees; Type: TABLE; Schema: hr; Owner: postgres
--

CREATE TABLE hr.employees (
    employee_id numeric(6,0) DEFAULT (0)::numeric NOT NULL,
    first_name character varying(20) DEFAULT NULL::character varying,
    last_name character varying(25) NOT NULL,
    email character varying(25) NOT NULL,
    phone_number character varying(20) DEFAULT NULL::character varying,
    hire_date date NOT NULL,
    job_id character varying(10) NOT NULL,
    salary numeric(8,2) DEFAULT NULL::numeric,
    commission_pct numeric(2,2),
    manager_id numeric(6,0),
    department_id numeric(4,0)
);


ALTER TABLE hr.employees OWNER TO postgres;

--
-- TOC entry 413 (class 1259 OID 415719)
-- Name: job_history; Type: TABLE; Schema: hr; Owner: postgres
--

CREATE TABLE hr.job_history (
    employee_id numeric(6,0) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    job_id character varying(10) NOT NULL,
    department_id numeric(4,0) DEFAULT NULL::numeric
);


ALTER TABLE hr.job_history OWNER TO postgres;

--
-- TOC entry 412 (class 1259 OID 415711)
-- Name: jobs; Type: TABLE; Schema: hr; Owner: postgres
--

CREATE TABLE hr.jobs (
    job_id character varying(10) DEFAULT ''::character varying NOT NULL,
    job_title character varying(35) NOT NULL,
    min_salary numeric(6,0) DEFAULT NULL::numeric,
    max_salary numeric(6,0) DEFAULT NULL::numeric
);


ALTER TABLE hr.jobs OWNER TO postgres;

--
-- TOC entry 410 (class 1259 OID 415694)
-- Name: locations; Type: TABLE; Schema: hr; Owner: postgres
--

CREATE TABLE hr.locations (
    location_id numeric(4,0) DEFAULT (0)::numeric NOT NULL,
    street_address character varying(40) DEFAULT NULL::character varying,
    postal_code character varying(12) DEFAULT NULL::character varying,
    city character varying(30) NOT NULL,
    state_province character varying(25) DEFAULT NULL::character varying,
    country_id character varying(2) DEFAULT NULL::character varying
);


ALTER TABLE hr.locations OWNER TO postgres;

--
-- TOC entry 409 (class 1259 OID 415689)
-- Name: regions; Type: TABLE; Schema: hr; Owner: postgres
--

CREATE TABLE hr.regions (
    region_id numeric(10,0) NOT NULL,
    region_name character varying(25)
);


ALTER TABLE hr.regions OWNER TO postgres;

--
-- TOC entry 3914 (class 0 OID 415684)
-- Dependencies: 408
-- Data for Name: countries; Type: TABLE DATA; Schema: hr; Owner: postgres
--

INSERT INTO hr.countries VALUES ('AR', 'Argentina', 2);
INSERT INTO hr.countries VALUES ('AU', 'Australia', 3);
INSERT INTO hr.countries VALUES ('BE', 'Belgium', 1);
INSERT INTO hr.countries VALUES ('BR', 'Brazil', 2);
INSERT INTO hr.countries VALUES ('CA', 'Canada', 2);
INSERT INTO hr.countries VALUES ('CH', 'Switzerland', 1);
INSERT INTO hr.countries VALUES ('CN', 'China', 3);
INSERT INTO hr.countries VALUES ('DE', 'Germany', 1);
INSERT INTO hr.countries VALUES ('DK', 'Denmark', 1);
INSERT INTO hr.countries VALUES ('EG', 'Egypt', 4);
INSERT INTO hr.countries VALUES ('FR', 'France', 1);
INSERT INTO hr.countries VALUES ('HK', 'HongKong', 3);
INSERT INTO hr.countries VALUES ('IL', 'Israel', 4);
INSERT INTO hr.countries VALUES ('IN', 'India', 3);
INSERT INTO hr.countries VALUES ('IT', 'Italy', 1);
INSERT INTO hr.countries VALUES ('JP', 'Japan', 3);
INSERT INTO hr.countries VALUES ('KW', 'Kuwait', 4);
INSERT INTO hr.countries VALUES ('MX', 'Mexico', 2);
INSERT INTO hr.countries VALUES ('NG', 'Nigeria', 4);
INSERT INTO hr.countries VALUES ('NL', 'Netherlands', 1);
INSERT INTO hr.countries VALUES ('SG', 'Singapore', 3);
INSERT INTO hr.countries VALUES ('UK', 'United Kingdom', 1);
INSERT INTO hr.countries VALUES ('US', 'United States of America', 2);
INSERT INTO hr.countries VALUES ('ZM', 'Zambia', 4);
INSERT INTO hr.countries VALUES ('ZW', 'Zimbabwe', 4);


--
-- TOC entry 3917 (class 0 OID 415704)
-- Dependencies: 411
-- Data for Name: departments; Type: TABLE DATA; Schema: hr; Owner: postgres
--

INSERT INTO hr.departments VALUES (10, 'Administration', 200, 1700);
INSERT INTO hr.departments VALUES (20, 'Marketing', 201, 1800);
INSERT INTO hr.departments VALUES (30, 'Purchasing', 114, 1700);
INSERT INTO hr.departments VALUES (40, 'Human Resources', 203, 2400);
INSERT INTO hr.departments VALUES (50, 'Shipping', 121, 1500);
INSERT INTO hr.departments VALUES (60, 'IT', 103, 1400);
INSERT INTO hr.departments VALUES (70, 'Public Relations', 204, 2700);
INSERT INTO hr.departments VALUES (80, 'Sales', 145, 2500);
INSERT INTO hr.departments VALUES (90, 'Executive', 100, 1700);
INSERT INTO hr.departments VALUES (100, 'Finance', 108, 1700);
INSERT INTO hr.departments VALUES (110, 'Accounting', 205, 1700);
INSERT INTO hr.departments VALUES (120, 'Treasury', NULL, 1700);
INSERT INTO hr.departments VALUES (130, 'Corporate Tax', NULL, 1700);
INSERT INTO hr.departments VALUES (140, 'Control And Credit', NULL, 1700);
INSERT INTO hr.departments VALUES (150, 'Shareholder Services', NULL, 1700);
INSERT INTO hr.departments VALUES (160, 'Benefits', NULL, 1700);
INSERT INTO hr.departments VALUES (170, 'Manufacturing', NULL, 1700);
INSERT INTO hr.departments VALUES (180, 'Construction', NULL, 1700);
INSERT INTO hr.departments VALUES (190, 'Contracting', NULL, 1700);
INSERT INTO hr.departments VALUES (200, 'Operations', NULL, 1700);
INSERT INTO hr.departments VALUES (210, 'IT Support', NULL, 1700);
INSERT INTO hr.departments VALUES (220, 'NOC', NULL, 1700);
INSERT INTO hr.departments VALUES (230, 'IT Helpdesk', NULL, 1700);
INSERT INTO hr.departments VALUES (240, 'Government Sales', NULL, 1700);
INSERT INTO hr.departments VALUES (250, 'Retail Sales', NULL, 1700);
INSERT INTO hr.departments VALUES (260, 'Recruiting', NULL, 1700);
INSERT INTO hr.departments VALUES (270, 'Payroll', NULL, 1700);


--
-- TOC entry 3913 (class 0 OID 415672)
-- Dependencies: 407
-- Data for Name: employees; Type: TABLE DATA; Schema: hr; Owner: postgres
--

INSERT INTO hr.employees VALUES (145, 'John', 'Russell', 'JRUSSEL', '011.44.1344.429268', '2004-10-01', 'SA_MAN', 14000.00, 0.40, 100, 80);
INSERT INTO hr.employees VALUES (146, 'Karen', 'Partners', 'KPARTNER', '011.44.1344.467268', '2005-01-05', 'SA_MAN', 13500.00, 0.30, 100, 80);
INSERT INTO hr.employees VALUES (147, 'Alberto', 'Errazuriz', 'AERRAZUR', '011.44.1344.429278', '2005-03-10', 'SA_MAN', 12000.00, 0.30, 100, 80);
INSERT INTO hr.employees VALUES (148, 'Gerald', 'Cambrault', 'GCAMBRAU', '011.44.1344.619268', '2007-10-15', 'SA_MAN', 11000.00, 0.30, 100, 80);
INSERT INTO hr.employees VALUES (149, 'Eleni', 'Zlotkey', 'EZLOTKEY', '011.44.1344.429018', '2008-01-29', 'SA_MAN', 10500.00, 0.20, 100, 80);
INSERT INTO hr.employees VALUES (150, 'Peter', 'Tucker', 'PTUCKER', '011.44.1344.129268', '2005-01-30', 'SA_REP', 10000.00, 0.30, 145, 80);
INSERT INTO hr.employees VALUES (151, 'David', 'Bernstein', 'DBERNSTE', '011.44.1344.345268', '2005-03-24', 'SA_REP', 9500.00, 0.25, 145, 80);
INSERT INTO hr.employees VALUES (152, 'Peter', 'Hall', 'PHALL', '011.44.1344.478968', '2005-08-20', 'SA_REP', 9000.00, 0.25, 145, 80);
INSERT INTO hr.employees VALUES (153, 'Christopher', 'Olsen', 'COLSEN', '011.44.1344.498718', '2006-03-30', 'SA_REP', 8000.00, 0.20, 145, 80);
INSERT INTO hr.employees VALUES (154, 'Nanette', 'Cambrault', 'NCAMBRAU', '011.44.1344.987668', '2006-12-09', 'SA_REP', 7500.00, 0.20, 145, 80);
INSERT INTO hr.employees VALUES (155, 'Oliver', 'Tuvault', 'OTUVAULT', '011.44.1344.486508', '2007-11-23', 'SA_REP', 7000.00, 0.15, 145, 80);
INSERT INTO hr.employees VALUES (156, 'Janette', 'King', 'JKING', '011.44.1345.429268', '2004-01-30', 'SA_REP', 10000.00, 0.35, 146, 80);
INSERT INTO hr.employees VALUES (157, 'Patrick', 'Sully', 'PSULLY', '011.44.1345.929268', '2004-03-04', 'SA_REP', 9500.00, 0.35, 146, 80);
INSERT INTO hr.employees VALUES (158, 'Allan', 'McEwen', 'AMCEWEN', '011.44.1345.829268', '2004-08-01', 'SA_REP', 9000.00, 0.35, 146, 80);
INSERT INTO hr.employees VALUES (159, 'Lindsey', 'Smith', 'LSMITH', '011.44.1345.729268', '2005-03-10', 'SA_REP', 8000.00, 0.30, 146, 80);
INSERT INTO hr.employees VALUES (160, 'Louise', 'Doran', 'LDORAN', '011.44.1345.629268', '2005-12-15', 'SA_REP', 7500.00, 0.30, 146, 80);
INSERT INTO hr.employees VALUES (161, 'Sarath', 'Sewall', 'SSEWALL', '011.44.1345.529268', '2006-11-03', 'SA_REP', 7000.00, 0.25, 146, 80);
INSERT INTO hr.employees VALUES (162, 'Clara', 'Vishney', 'CVISHNEY', '011.44.1346.129268', '2005-11-11', 'SA_REP', 10500.00, 0.25, 147, 80);
INSERT INTO hr.employees VALUES (163, 'Danielle', 'Greene', 'DGREENE', '011.44.1346.229268', '2007-03-19', 'SA_REP', 9500.00, 0.15, 147, 80);
INSERT INTO hr.employees VALUES (164, 'Mattea', 'Marvins', 'MMARVINS', '011.44.1346.329268', '2008-01-24', 'SA_REP', 7200.00, 0.10, 147, 80);
INSERT INTO hr.employees VALUES (165, 'David', 'Lee', 'DLEE', '011.44.1346.529268', '2008-02-23', 'SA_REP', 6800.00, 0.10, 147, 80);
INSERT INTO hr.employees VALUES (166, 'Sundar', 'Ande', 'SANDE', '011.44.1346.629268', '2008-03-24', 'SA_REP', 6400.00, 0.10, 147, 80);
INSERT INTO hr.employees VALUES (167, 'Amit', 'Banda', 'ABANDA', '011.44.1346.729268', '2008-04-21', 'SA_REP', 6200.00, 0.10, 147, 80);
INSERT INTO hr.employees VALUES (168, 'Lisa', 'Ozer', 'LOZER', '011.44.1343.929268', '2005-03-11', 'SA_REP', 11500.00, 0.25, 148, 80);
INSERT INTO hr.employees VALUES (169, 'Harrison', 'Bloom', 'HBLOOM', '011.44.1343.829268', '2006-03-23', 'SA_REP', 10000.00, 0.20, 148, 80);
INSERT INTO hr.employees VALUES (170, 'Tayler', 'Fox', 'TFOX', '011.44.1343.729268', '2006-01-24', 'SA_REP', 9600.00, 0.20, 148, 80);
INSERT INTO hr.employees VALUES (171, 'William', 'Smith', 'WSMITH', '011.44.1343.629268', '2007-02-23', 'SA_REP', 7400.00, 0.15, 148, 80);
INSERT INTO hr.employees VALUES (172, 'Elizabeth', 'Bates', 'EBATES', '011.44.1343.529268', '2007-03-24', 'SA_REP', 7300.00, 0.15, 148, 80);
INSERT INTO hr.employees VALUES (173, 'Sundita', 'Kumar', 'SKUMAR', '011.44.1343.329268', '2008-04-21', 'SA_REP', 6100.00, 0.10, 148, 80);
INSERT INTO hr.employees VALUES (174, 'Ellen', 'Abel', 'EABEL', '011.44.1644.429267', '2004-05-11', 'SA_REP', 11000.00, 0.30, 149, 80);
INSERT INTO hr.employees VALUES (175, 'Alyssa', 'Hutton', 'AHUTTON', '011.44.1644.429266', '2005-03-19', 'SA_REP', 8800.00, 0.25, 149, 80);
INSERT INTO hr.employees VALUES (176, 'Jonathon', 'Taylor', 'JTAYLOR', '011.44.1644.429265', '2006-03-24', 'SA_REP', 8600.00, 0.20, 149, 80);
INSERT INTO hr.employees VALUES (177, 'Jack', 'Livingston', 'JLIVINGS', '011.44.1644.429264', '2006-04-23', 'SA_REP', 8400.00, 0.20, 149, 80);
INSERT INTO hr.employees VALUES (179, 'Charles', 'Johnson', 'CJOHNSON', '011.44.1644.429262', '2008-01-04', 'SA_REP', 6200.00, 0.10, 149, 80);
INSERT INTO hr.employees VALUES (178, 'Kimberely', 'Grant', 'KGRANT', '011.44.1644.429263', '2007-05-24', 'SA_REP', 7000.00, 0.15, 149, NULL);
INSERT INTO hr.employees VALUES (101, 'Neena', 'Kochhar', 'NKOCHHAR', '515.123.4568', '2005-09-21', 'AD_VP', 17000.00, NULL, 100, 90);
INSERT INTO hr.employees VALUES (102, 'Lex', 'De Haan', 'LDEHAAN', '515.123.4569', '2001-01-13', 'AD_VP', 17000.00, NULL, 100, 90);
INSERT INTO hr.employees VALUES (103, 'Alexander', 'Hunold', 'AHUNOLD', '590.423.4567', '2006-01-03', 'IT_PROG', 9000.00, NULL, 102, 60);
INSERT INTO hr.employees VALUES (104, 'Bruce', 'Ernst', 'BERNST', '590.423.4568', '2007-05-21', 'IT_PROG', 6000.00, NULL, 103, 60);
INSERT INTO hr.employees VALUES (105, 'David', 'Austin', 'DAUSTIN', '590.423.4569', '2005-06-25', 'IT_PROG', 4800.00, NULL, 103, 60);
INSERT INTO hr.employees VALUES (106, 'Valli', 'Pataballa', 'VPATABAL', '590.423.4560', '2006-02-05', 'IT_PROG', 4800.00, NULL, 103, 60);
INSERT INTO hr.employees VALUES (107, 'Diana', 'Lorentz', 'DLORENTZ', '590.423.5567', '2007-02-07', 'IT_PROG', 4200.00, NULL, 103, 60);
INSERT INTO hr.employees VALUES (108, 'Nancy', 'Greenberg', 'NGREENBE', '515.124.4569', '2002-08-17', 'FI_MGR', 12000.00, NULL, 101, 100);
INSERT INTO hr.employees VALUES (109, 'Daniel', 'Faviet', 'DFAVIET', '515.124.4169', '2002-08-16', 'FI_ACCOUNT', 9000.00, NULL, 108, 100);
INSERT INTO hr.employees VALUES (110, 'John', 'Chen', 'JCHEN', '515.124.4269', '2005-09-28', 'FI_ACCOUNT', 8200.00, NULL, 108, 100);
INSERT INTO hr.employees VALUES (111, 'Ismael', 'Sciarra', 'ISCIARRA', '515.124.4369', '2005-09-30', 'FI_ACCOUNT', 7700.00, NULL, 108, 100);
INSERT INTO hr.employees VALUES (112, 'Jose Manuel', 'Urman', 'JMURMAN', '515.124.4469', '2006-03-07', 'FI_ACCOUNT', 7800.00, NULL, 108, 100);
INSERT INTO hr.employees VALUES (113, 'Luis', 'Popp', 'LPOPP', '515.124.4567', '2007-12-07', 'FI_ACCOUNT', 6900.00, NULL, 108, 100);
INSERT INTO hr.employees VALUES (114, 'Den', 'Raphaely', 'DRAPHEAL', '515.127.4561', '2002-12-07', 'PU_MAN', 11000.00, NULL, 100, 30);
INSERT INTO hr.employees VALUES (115, 'Alexander', 'Khoo', 'AKHOO', '515.127.4562', '2003-05-18', 'PU_CLERK', 3100.00, NULL, 114, 30);
INSERT INTO hr.employees VALUES (116, 'Shelli', 'Baida', 'SBAIDA', '515.127.4563', '2005-12-24', 'PU_CLERK', 2900.00, NULL, 114, 30);
INSERT INTO hr.employees VALUES (117, 'Sigal', 'Tobias', 'STOBIAS', '515.127.4564', '2005-07-24', 'PU_CLERK', 2800.00, NULL, 114, 30);
INSERT INTO hr.employees VALUES (118, 'Guy', 'Himuro', 'GHIMURO', '515.127.4565', '2006-11-15', 'PU_CLERK', 2600.00, NULL, 114, 30);
INSERT INTO hr.employees VALUES (119, 'Karen', 'Colmenares', 'KCOLMENA', '515.127.4566', '2007-08-10', 'PU_CLERK', 2500.00, NULL, 114, 30);
INSERT INTO hr.employees VALUES (120, 'Matthew', 'Weiss', 'MWEISS', '650.123.1234', '2004-07-18', 'ST_MAN', 8000.00, NULL, 100, 50);
INSERT INTO hr.employees VALUES (121, 'Adam', 'Fripp', 'AFRIPP', '650.123.2234', '2005-04-10', 'ST_MAN', 8200.00, NULL, 100, 50);
INSERT INTO hr.employees VALUES (122, 'Payam', 'Kaufling', 'PKAUFLIN', '650.123.3234', '2003-05-01', 'ST_MAN', 7900.00, NULL, 100, 50);
INSERT INTO hr.employees VALUES (123, 'Shanta', 'Vollman', 'SVOLLMAN', '650.123.4234', '2005-10-10', 'ST_MAN', 6500.00, NULL, 100, 50);
INSERT INTO hr.employees VALUES (124, 'Kevin', 'Mourgos', 'KMOURGOS', '650.123.5234', '2007-11-16', 'ST_MAN', 5800.00, NULL, 100, 50);
INSERT INTO hr.employees VALUES (125, 'Julia', 'Nayer', 'JNAYER', '650.124.1214', '2005-07-16', 'ST_CLERK', 3200.00, NULL, 120, 50);
INSERT INTO hr.employees VALUES (126, 'Irene', 'Mikkilineni', 'IMIKKILI', '650.124.1224', '2006-09-28', 'ST_CLERK', 2700.00, NULL, 120, 50);
INSERT INTO hr.employees VALUES (127, 'James', 'Landry', 'JLANDRY', '650.124.1334', '2007-01-14', 'ST_CLERK', 2400.00, NULL, 120, 50);
INSERT INTO hr.employees VALUES (128, 'Steven', 'Markle', 'SMARKLE', '650.124.1434', '2008-03-08', 'ST_CLERK', 2200.00, NULL, 120, 50);
INSERT INTO hr.employees VALUES (129, 'Laura', 'Bissot', 'LBISSOT', '650.124.5234', '2005-08-20', 'ST_CLERK', 3300.00, NULL, 121, 50);
INSERT INTO hr.employees VALUES (130, 'Mozhe', 'Atkinson', 'MATKINSO', '650.124.6234', '2005-10-30', 'ST_CLERK', 2800.00, NULL, 121, 50);
INSERT INTO hr.employees VALUES (131, 'James', 'Marlow', 'JAMRLOW', '650.124.7234', '2005-02-16', 'ST_CLERK', 2500.00, NULL, 121, 50);
INSERT INTO hr.employees VALUES (132, 'TJ', 'Olson', 'TJOLSON', '650.124.8234', '2007-04-10', 'ST_CLERK', 2100.00, NULL, 121, 50);
INSERT INTO hr.employees VALUES (133, 'Jason', 'Mallin', 'JMALLIN', '650.127.1934', '2004-06-14', 'ST_CLERK', 3300.00, NULL, 122, 50);
INSERT INTO hr.employees VALUES (134, 'Michael', 'Rogers', 'MROGERS', '650.127.1834', '2006-08-26', 'ST_CLERK', 2900.00, NULL, 122, 50);
INSERT INTO hr.employees VALUES (135, 'Ki', 'Gee', 'KGEE', '650.127.1734', '2007-12-12', 'ST_CLERK', 2400.00, NULL, 122, 50);
INSERT INTO hr.employees VALUES (136, 'Hazel', 'Philtanker', 'HPHILTAN', '650.127.1634', '2008-02-06', 'ST_CLERK', 2200.00, NULL, 122, 50);
INSERT INTO hr.employees VALUES (137, 'Renske', 'Ladwig', 'RLADWIG', '650.121.1234', '2003-07-14', 'ST_CLERK', 3600.00, NULL, 123, 50);
INSERT INTO hr.employees VALUES (138, 'Stephen', 'Stiles', 'SSTILES', '650.121.2034', '2005-10-26', 'ST_CLERK', 3200.00, NULL, 123, 50);
INSERT INTO hr.employees VALUES (139, 'John', 'Seo', 'JSEO', '650.121.2019', '2006-02-12', 'ST_CLERK', 2700.00, NULL, 123, 50);
INSERT INTO hr.employees VALUES (140, 'Joshua', 'Patel', 'JPATEL', '650.121.1834', '2006-04-06', 'ST_CLERK', 2500.00, NULL, 123, 50);
INSERT INTO hr.employees VALUES (141, 'Trenna', 'Rajs', 'TRAJS', '650.121.8009', '2003-10-17', 'ST_CLERK', 3500.00, NULL, 124, 50);
INSERT INTO hr.employees VALUES (142, 'Curtis', 'Davies', 'CDAVIES', '650.121.2994', '2005-01-29', 'ST_CLERK', 3100.00, NULL, 124, 50);
INSERT INTO hr.employees VALUES (143, 'Randall', 'Matos', 'RMATOS', '650.121.2874', '2006-03-15', 'ST_CLERK', 2600.00, NULL, 124, 50);
INSERT INTO hr.employees VALUES (144, 'Peter', 'Vargas', 'PVARGAS', '650.121.2004', '2006-07-09', 'ST_CLERK', 2500.00, NULL, 124, 50);
INSERT INTO hr.employees VALUES (180, 'Winston', 'Taylor', 'WTAYLOR', '650.507.9876', '2006-01-24', 'SH_CLERK', 3200.00, NULL, 120, 50);
INSERT INTO hr.employees VALUES (100, 'Steven', 'King', 'SKING', '515.123.4567', '2003-06-17', 'AD_PRES', 24000.00, NULL, NULL, 90);
INSERT INTO hr.employees VALUES (181, 'Jean', 'Fleaur', 'JFLEAUR', '650.507.9877', '2006-02-23', 'SH_CLERK', 3100.00, NULL, 120, 50);
INSERT INTO hr.employees VALUES (182, 'Martha', 'Sullivan', 'MSULLIVA', '650.507.9878', '2007-06-21', 'SH_CLERK', 2500.00, NULL, 120, 50);
INSERT INTO hr.employees VALUES (183, 'Girard', 'Geoni', 'GGEONI', '650.507.9879', '2008-02-03', 'SH_CLERK', 2800.00, NULL, 120, 50);
INSERT INTO hr.employees VALUES (184, 'Nandita', 'Sarchand', 'NSARCHAN', '650.509.1876', '2004-01-27', 'SH_CLERK', 4200.00, NULL, 121, 50);
INSERT INTO hr.employees VALUES (185, 'Alexis', 'Bull', 'ABULL', '650.509.2876', '2005-02-20', 'SH_CLERK', 4100.00, NULL, 121, 50);
INSERT INTO hr.employees VALUES (186, 'Julia', 'Dellinger', 'JDELLING', '650.509.3876', '2006-06-24', 'SH_CLERK', 3400.00, NULL, 121, 50);
INSERT INTO hr.employees VALUES (187, 'Anthony', 'Cabrio', 'ACABRIO', '650.509.4876', '2007-02-07', 'SH_CLERK', 3000.00, NULL, 121, 50);
INSERT INTO hr.employees VALUES (188, 'Kelly', 'Chung', 'KCHUNG', '650.505.1876', '2005-06-14', 'SH_CLERK', 3800.00, NULL, 122, 50);
INSERT INTO hr.employees VALUES (189, 'Jennifer', 'Dilly', 'JDILLY', '650.505.2876', '2005-08-13', 'SH_CLERK', 3600.00, NULL, 122, 50);
INSERT INTO hr.employees VALUES (190, 'Timothy', 'Gates', 'TGATES', '650.505.3876', '2006-07-11', 'SH_CLERK', 2900.00, NULL, 122, 50);
INSERT INTO hr.employees VALUES (191, 'Randall', 'Perkins', 'RPERKINS', '650.505.4876', '2007-12-19', 'SH_CLERK', 2500.00, NULL, 122, 50);
INSERT INTO hr.employees VALUES (192, 'Sarah', 'Bell', 'SBELL', '650.501.1876', '2004-02-04', 'SH_CLERK', 4000.00, NULL, 123, 50);
INSERT INTO hr.employees VALUES (193, 'Britney', 'Everett', 'BEVERETT', '650.501.2876', '2005-03-03', 'SH_CLERK', 3900.00, NULL, 123, 50);
INSERT INTO hr.employees VALUES (194, 'Samuel', 'McCain', 'SMCCAIN', '650.501.3876', '2006-07-01', 'SH_CLERK', 3200.00, NULL, 123, 50);
INSERT INTO hr.employees VALUES (195, 'Vance', 'Jones', 'VJONES', '650.501.4876', '2007-03-17', 'SH_CLERK', 2800.00, NULL, 123, 50);
INSERT INTO hr.employees VALUES (196, 'Alana', 'Walsh', 'AWALSH', '650.507.9811', '2006-04-24', 'SH_CLERK', 3100.00, NULL, 124, 50);
INSERT INTO hr.employees VALUES (197, 'Kevin', 'Feeney', 'KFEENEY', '650.507.9822', '2006-05-23', 'SH_CLERK', 3000.00, NULL, 124, 50);
INSERT INTO hr.employees VALUES (198, 'Donald', 'OConnell', 'DOCONNEL', '650.507.9833', '2007-06-21', 'SH_CLERK', 2600.00, NULL, 124, 50);
INSERT INTO hr.employees VALUES (199, 'Douglas', 'Grant', 'DGRANT', '650.507.9844', '2008-01-13', 'SH_CLERK', 2600.00, NULL, 124, 50);
INSERT INTO hr.employees VALUES (200, 'Jennifer', 'Whalen', 'JWHALEN', '515.123.4444', '2003-09-17', 'AD_ASST', 4400.00, NULL, 101, 10);
INSERT INTO hr.employees VALUES (201, 'Michael', 'Hartstein', 'MHARTSTE', '515.123.5555', '2004-02-17', 'MK_MAN', 13000.00, NULL, 100, 20);
INSERT INTO hr.employees VALUES (202, 'Pat', 'Fay', 'PFAY', '603.123.6666', '2005-08-17', 'MK_REP', 6000.00, NULL, 201, 20);
INSERT INTO hr.employees VALUES (203, 'Susan', 'Mavris', 'SMAVRIS', '515.123.7777', '2002-06-07', 'HR_REP', 6500.00, NULL, 101, 40);
INSERT INTO hr.employees VALUES (204, 'Hermann', 'Baer', 'HBAER', '515.123.8888', '2002-06-07', 'PR_REP', 10000.00, NULL, 101, 70);
INSERT INTO hr.employees VALUES (205, 'Shelley', 'Higgins', 'SHIGGINS', '515.123.8080', '2002-06-07', 'AC_MGR', 12000.00, NULL, 101, 110);
INSERT INTO hr.employees VALUES (206, 'William', 'Gietz', 'WGIETZ', '515.123.8181', '2002-06-07', 'AC_ACCOUNT', 8300.00, NULL, 205, 110);


--
-- TOC entry 3919 (class 0 OID 415719)
-- Dependencies: 413
-- Data for Name: job_history; Type: TABLE DATA; Schema: hr; Owner: postgres
--

INSERT INTO hr.job_history VALUES (201, '2004-02-17', '2007-12-19', 'MK_REP', 20);
INSERT INTO hr.job_history VALUES (200, '2002-07-01', '2006-12-31', 'AC_ACCOUNT', 90);
INSERT INTO hr.job_history VALUES (200, '1995-09-17', '2001-06-17', 'AD_ASST', 90);
INSERT INTO hr.job_history VALUES (176, '2007-01-01', '2007-12-31', 'SA_MAN', 80);
INSERT INTO hr.job_history VALUES (176, '2006-03-24', '2006-12-31', 'SA_REP', 80);
INSERT INTO hr.job_history VALUES (122, '2007-01-01', '2007-12-31', 'ST_CLERK', 50);
INSERT INTO hr.job_history VALUES (114, '2006-03-24', '2007-12-31', 'ST_CLERK', 50);
INSERT INTO hr.job_history VALUES (102, '2001-01-13', '2006-07-24', 'IT_PROG', 60);
INSERT INTO hr.job_history VALUES (101, '2001-10-28', '2005-03-15', 'AC_MGR', 110);
INSERT INTO hr.job_history VALUES (101, '1997-09-21', '2001-10-27', 'AC_ACCOUNT', 110);
INSERT INTO hr.job_history VALUES (102, '1993-01-13', '1998-07-24', 'IT_PROG', 60);
INSERT INTO hr.job_history VALUES (101, '1989-09-21', '1993-10-27', 'AC_ACCOUNT', 110);
INSERT INTO hr.job_history VALUES (101, '1993-10-28', '1997-03-15', 'AC_MGR', 110);
INSERT INTO hr.job_history VALUES (201, '1996-02-17', '1999-12-19', 'MK_REP', 20);
INSERT INTO hr.job_history VALUES (114, '1998-03-24', '1999-12-31', 'ST_CLERK', 50);
INSERT INTO hr.job_history VALUES (122, '1999-01-01', '1999-12-31', 'ST_CLERK', 50);
INSERT INTO hr.job_history VALUES (200, '1987-09-17', '1993-06-17', 'AD_ASST', 90);
INSERT INTO hr.job_history VALUES (176, '1998-03-24', '1998-12-31', 'SA_REP', 80);
INSERT INTO hr.job_history VALUES (176, '1999-01-01', '1999-12-31', 'SA_MAN', 80);
INSERT INTO hr.job_history VALUES (200, '1994-07-01', '1998-12-31', 'AC_ACCOUNT', 90);


--
-- TOC entry 3918 (class 0 OID 415711)
-- Dependencies: 412
-- Data for Name: jobs; Type: TABLE DATA; Schema: hr; Owner: postgres
--

INSERT INTO hr.jobs VALUES ('AD_PRES', 'President', 20000, 40000);
INSERT INTO hr.jobs VALUES ('AD_VP', 'Administration Vice President', 15000, 30000);
INSERT INTO hr.jobs VALUES ('AD_ASST', 'Administration Assistant', 3000, 6000);
INSERT INTO hr.jobs VALUES ('FI_MGR', 'Finance Manager', 8200, 16000);
INSERT INTO hr.jobs VALUES ('FI_ACCOUNT', 'Accountant', 4200, 9000);
INSERT INTO hr.jobs VALUES ('AC_MGR', 'Accounting Manager', 8200, 16000);
INSERT INTO hr.jobs VALUES ('AC_ACCOUNT', 'Public Accountant', 4200, 9000);
INSERT INTO hr.jobs VALUES ('SA_MAN', 'Sales Manager', 10000, 20000);
INSERT INTO hr.jobs VALUES ('SA_REP', 'Sales Representative', 6000, 12000);
INSERT INTO hr.jobs VALUES ('PU_MAN', 'Purchasing Manager', 8000, 15000);
INSERT INTO hr.jobs VALUES ('PU_CLERK', 'Purchasing Clerk', 2500, 5500);
INSERT INTO hr.jobs VALUES ('ST_MAN', 'Stock Manager', 5500, 8500);
INSERT INTO hr.jobs VALUES ('ST_CLERK', 'Stock Clerk', 2000, 5000);
INSERT INTO hr.jobs VALUES ('SH_CLERK', 'Shipping Clerk', 2500, 5500);
INSERT INTO hr.jobs VALUES ('IT_PROG', 'Programmer', 4000, 10000);
INSERT INTO hr.jobs VALUES ('MK_MAN', 'Marketing Manager', 9000, 15000);
INSERT INTO hr.jobs VALUES ('MK_REP', 'Marketing Representative', 4000, 9000);
INSERT INTO hr.jobs VALUES ('HR_REP', 'Human Resources Representative', 4000, 9000);
INSERT INTO hr.jobs VALUES ('PR_REP', 'Public Relations Representative', 4500, 10500);


--
-- TOC entry 3916 (class 0 OID 415694)
-- Dependencies: 410
-- Data for Name: locations; Type: TABLE DATA; Schema: hr; Owner: postgres
--

INSERT INTO hr.locations VALUES (1200, '2017 Shinjuku-ku', '1689', 'Tokyo', 'Tokyo Prefecture', 'JP');
INSERT INTO hr.locations VALUES (1400, '2014 Jabberwocky Rd', '26192', 'Southlake', 'Texas', 'US');
INSERT INTO hr.locations VALUES (1500, '2011 Interiors Blvd', '99236', 'South San Francisco', 'California', 'US');
INSERT INTO hr.locations VALUES (1600, '2007 Zagora St', '50090', 'South Brunswick', 'New Jersey', 'US');
INSERT INTO hr.locations VALUES (1700, '2004 Charade Rd', '98199', 'Seattle', 'Washington', 'US');
INSERT INTO hr.locations VALUES (1800, '147 Spadina Ave', 'M5V 2L7', 'Toronto', 'Ontario', 'CA');
INSERT INTO hr.locations VALUES (1900, '6092 Boxwood St', 'YSW 9T2', 'Whitehorse', 'Yukon', 'CA');
INSERT INTO hr.locations VALUES (2100, '1298 Vileparle (E)', '490231', 'Bombay', 'Maharashtra', 'IN');
INSERT INTO hr.locations VALUES (2200, '12-98 Victoria Street', '2901', 'Sydney', 'New South Wales', 'AU');
INSERT INTO hr.locations VALUES (2600, '9702 Chester Road', '9629850293', 'Stretford', 'Manchester', 'UK');
INSERT INTO hr.locations VALUES (2700, 'Schwanthalerstr. 7031', '80925', 'Munich', 'Bavaria', 'DE');
INSERT INTO hr.locations VALUES (2800, 'Rua Frei Caneca 1360', '01307-002', 'Sao Paulo', 'Sao Paulo', 'BR');
INSERT INTO hr.locations VALUES (2900, '20 Rue des Corps-Saints', '1730', 'Geneva', 'Geneve', 'CH');
INSERT INTO hr.locations VALUES (3000, 'Murtenstrasse 921', '3095', 'Bern', 'BE', 'CH');
INSERT INTO hr.locations VALUES (3100, 'Pieter Breughelstraat 837', '3029SK', 'Utrecht', 'Utrecht', 'NL');
INSERT INTO hr.locations VALUES (1000, '1297 Via Cola di Rie', '989', 'Roma', NULL, 'IT');
INSERT INTO hr.locations VALUES (1100, '93091 Calle della Testa', '10934', 'Venice', NULL, 'IT');
INSERT INTO hr.locations VALUES (1300, '9450 Kamiya-cho', '6823', 'Hiroshima', NULL, 'JP');
INSERT INTO hr.locations VALUES (2000, '40-5-12 Laogianggen', '190518', 'Beijing', NULL, 'CN');
INSERT INTO hr.locations VALUES (2300, '198 Clementi North', '540198', 'Singapore', NULL, 'SG');
INSERT INTO hr.locations VALUES (2400, '8204 Arthur St', NULL, 'London', NULL, 'UK');
INSERT INTO hr.locations VALUES (3200, 'Mariano Escobedo 9991', '11932', 'Mexico City', 'Distrito Federal', 'MX');
INSERT INTO hr.locations VALUES (2500, 'Magdalen Centre, The Oxford Science Park', 'OX9 9ZB', 'Oxford', 'Oxford', 'UK');


--
-- TOC entry 3915 (class 0 OID 415689)
-- Dependencies: 409
-- Data for Name: regions; Type: TABLE DATA; Schema: hr; Owner: postgres
--

INSERT INTO hr.regions VALUES (1, 'Europe                   ');
INSERT INTO hr.regions VALUES (2, 'Americas                 ');
INSERT INTO hr.regions VALUES (3, 'Asia                     ');
INSERT INTO hr.regions VALUES (4, 'Middle East and Africa   ');


--
-- TOC entry 3728 (class 2606 OID 415688)
-- Name: countries pk_countries; Type: CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.countries
    ADD CONSTRAINT pk_countries PRIMARY KEY (country_id);


--
-- TOC entry 3737 (class 2606 OID 415710)
-- Name: departments pk_departments; Type: CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.departments
    ADD CONSTRAINT pk_departments PRIMARY KEY (department_id);


--
-- TOC entry 3725 (class 2606 OID 415683)
-- Name: employees pk_employees; Type: CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT pk_employees PRIMARY KEY (employee_id);


--
-- TOC entry 3743 (class 2606 OID 415724)
-- Name: job_history pk_job_history; Type: CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.job_history
    ADD CONSTRAINT pk_job_history PRIMARY KEY (employee_id, start_date);


--
-- TOC entry 3739 (class 2606 OID 415718)
-- Name: jobs pk_jobs; Type: CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.jobs
    ADD CONSTRAINT pk_jobs PRIMARY KEY (job_id);


--
-- TOC entry 3733 (class 2606 OID 415703)
-- Name: locations pk_locations; Type: CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.locations
    ADD CONSTRAINT pk_locations PRIMARY KEY (location_id);


--
-- TOC entry 3730 (class 2606 OID 415693)
-- Name: regions pk_regions; Type: CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.regions
    ADD CONSTRAINT pk_regions PRIMARY KEY (region_id);


--
-- TOC entry 3726 (class 1259 OID 415730)
-- Name: fki_fk_countries_regions; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_fk_countries_regions ON hr.countries USING btree (region_id);


--
-- TOC entry 3734 (class 1259 OID 415776)
-- Name: fki_fk_departments_employees; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_fk_departments_employees ON hr.departments USING btree (manager_id);


--
-- TOC entry 3735 (class 1259 OID 415765)
-- Name: fki_fk_departments_locations; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_fk_departments_locations ON hr.departments USING btree (location_id);


--
-- TOC entry 3721 (class 1259 OID 415759)
-- Name: fki_fk_employees_departments; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_fk_employees_departments ON hr.employees USING btree (department_id);


--
-- TOC entry 3722 (class 1259 OID 415747)
-- Name: fki_fk_employees_manager; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_fk_employees_manager ON hr.employees USING btree (manager_id);


--
-- TOC entry 3723 (class 1259 OID 415753)
-- Name: fki_fk_empoyees_jobs; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_fk_empoyees_jobs ON hr.employees USING btree (job_id);


--
-- TOC entry 3731 (class 1259 OID 415741)
-- Name: fki_fk_locations_countries; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_fk_locations_countries ON hr.locations USING btree (country_id);


--
-- TOC entry 3740 (class 1259 OID 415788)
-- Name: fki_pk_job_history_departments; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_pk_job_history_departments ON hr.job_history USING btree (department_id);


--
-- TOC entry 3741 (class 1259 OID 415782)
-- Name: fki_pk_job_history_jobs; Type: INDEX; Schema: hr; Owner: postgres
--

CREATE INDEX fki_pk_job_history_jobs ON hr.job_history USING btree (job_id);


--
-- TOC entry 3747 (class 2606 OID 415725)
-- Name: countries fk_countries_regions; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.countries
    ADD CONSTRAINT fk_countries_regions FOREIGN KEY (region_id) REFERENCES hr.regions(region_id);


--
-- TOC entry 3750 (class 2606 OID 415771)
-- Name: departments fk_departments_employees; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.departments
    ADD CONSTRAINT fk_departments_employees FOREIGN KEY (manager_id) REFERENCES hr.employees(employee_id);


--
-- TOC entry 3749 (class 2606 OID 415760)
-- Name: departments fk_departments_locations; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.departments
    ADD CONSTRAINT fk_departments_locations FOREIGN KEY (location_id) REFERENCES hr.locations(location_id);


--
-- TOC entry 3746 (class 2606 OID 415754)
-- Name: employees fk_employees_departments; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT fk_employees_departments FOREIGN KEY (department_id) REFERENCES hr.departments(department_id);


--
-- TOC entry 3744 (class 2606 OID 415742)
-- Name: employees fk_employees_manager; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT fk_employees_manager FOREIGN KEY (manager_id) REFERENCES hr.employees(employee_id);


--
-- TOC entry 3745 (class 2606 OID 415748)
-- Name: employees fk_empoyees_jobs; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT fk_empoyees_jobs FOREIGN KEY (job_id) REFERENCES hr.jobs(job_id);


--
-- TOC entry 3748 (class 2606 OID 415736)
-- Name: locations fk_locations_countries; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.locations
    ADD CONSTRAINT fk_locations_countries FOREIGN KEY (country_id) REFERENCES hr.countries(country_id);


--
-- TOC entry 3752 (class 2606 OID 415783)
-- Name: job_history pk_job_history_departments; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.job_history
    ADD CONSTRAINT pk_job_history_departments FOREIGN KEY (department_id) REFERENCES hr.departments(department_id);


--
-- TOC entry 3751 (class 2606 OID 415777)
-- Name: job_history pk_job_history_jobs; Type: FK CONSTRAINT; Schema: hr; Owner: postgres
--

ALTER TABLE ONLY hr.job_history
    ADD CONSTRAINT pk_job_history_jobs FOREIGN KEY (job_id) REFERENCES hr.jobs(job_id);


-- Completed on 2021-12-29 17:51:36

--
-- PostgreSQL database dump complete
--

